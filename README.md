# Discord GIF avatar annihilator

**Note: I know nothing about JavaScript. I quickly hacked this together based on information from the internet.**

This removes the play-gif-avatar-when-message-hovered-over behavior on users with Discord Nitro who have uploaded gif avatars.  
They will still play if you click on the profile picture to display the user's profile.

# Installation

1. Extract `app.asar`

2. In `index.js`, under these two lines:
    ```
    mainWindow = new BrowserWindow(mainWindowOptions);
    global.mainWindowId = mainWindow.id;
    ```
    add this line:
    ```
    require('gif-avatar-annihilator').prepare(mainWindow);
    ```

3. Put this repo inside `node_modules` with the name `gif-avatar-annihilator`

4. Rename the old `app.asar` to something else, like `_app.asar`

5. Move the extracted modified directory into `app` in the same directory as the old `app.asar`

6. Done
