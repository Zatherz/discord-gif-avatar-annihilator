function prepare(window) {
    window.webContents.executeJavaScript(`
    var annihilate_nitro = function(image) {
      var url_raw = image.style.backgroundImage;
      var url = url_raw.substring(4, url_raw.length - 1);
  
      var new_url = url.replace(".gif", ".jpg");
      var new_raw_url = "url(" + new_url + ")"
      image.style.backgroundImage = new_raw_url;
    }

      var annihilate_child = function(me) {
      var elems = me.getElementsByClassName("avatar-large");
      annihilate_nitro(elems[0]);
    }

    setInterval(function() {
      
      var elems = window.document.getElementsByClassName("message-group");
      for (i = 0; i < elems.length; i++) {
        elems[i].setAttribute("onmouseover", "annihilate_child(this)");
        elems[i].getElementsByClassName("avatar-large")[0].className = "avatar-large";
    }}, 0);

    console.log("GIF avatars will be annihilated.");
    `);
}

exports.prepare = prepare;
